import static org.junit.Assert.*;

import org.junit.Test;

public class MatrixTest {

	@Test
	public void test() {
		TwoByTwoMatrix a = new TwoByTwoMatrix(4, 4, 2, 2);
		TwoByTwoMatrix b = new TwoByTwoMatrix(1, 2, 3, 4);
		TwoByTwoMatrix bInverse = new TwoByTwoMatrix(-2, 1, 1.5, -0.5);
		TwoByTwoMatrix c = new TwoByTwoMatrix(2, 3, 4, 4);
		TwoByTwoMatrix productAandB = new TwoByTwoMatrix(16, 24, 8, 12);
		assertEquals(a.inverse(), null);
		assertFalse(b.inverse().equals(c));
		assertTrue(b.inverse().equals(bInverse));
		assertEquals(productAandB, a.multiply(b));
	}

}
